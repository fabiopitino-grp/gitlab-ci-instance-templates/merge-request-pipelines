# Merge Request pipelines

Trigger pipelines for merge requests as well as ensure the pipelines run for tags and default branch.

## How to use it

Copy-paste the following:

```yaml
include:
  - project: fabiopitino-grp/gitlab-ci-instance-templates/merge-request-pipelines
    file: .gitlab-ci.yml
```
